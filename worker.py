import redis, uuid, time, datetime, ast
from hotqueue import HotQueue

q = HotQueue("queue", host='172.17.0.1', port=6379, db=0)
rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=1)


def generate_jid():
    return str(uuid.uuid4())

def generate_job_key(jid):
    return 'job.{}'.format(jid)

def instantiate_job(jid, status, start, end, time):
    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'start': start,
                'end': end,
                'time': time,
               }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'start': start.decode('utf-8'),
            'end': end.decode('utf-8'),
            'time': time.decode('utf-8')
           }

def __save_job(job_key, job_dict):
    """Save a job object in the Redis database."""
    rd.set(job_key, str(job_dict))

def __queue_job(jid):
    """Add a jid to the redis queue"""
    q.put(jid)

def add_job(start, end, status="submitted"):
    """Add an entire job to the redis database"""
    time = datetime.datetime.now()
    jid = generate_jid()
    job_key = generate_job_key(jid)
    job_dict = instantiate_job(jid, status, start, end, str(time))
    __save_job(job_key, job_dict)
    __queue_job(jid)
    return job_key


def get_status(job_key):
    try:
        job = ast.literal_eval(rd.get(job_key))
        rd.set(job_key, str(job))
        return job["status"]
    except:
        return ("Invalid Request")


#key = add_job(1, 2)
#print(get_status(key))



