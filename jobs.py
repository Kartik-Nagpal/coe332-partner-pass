import redis, json, worker, ast
from hotqueue import HotQueue

q = HotQueue("queue", host='172.17.0.1', port=6379, db=0)
rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=1)

@q.worker
def execute_job(jid):
    if(jid is not None):
        job_key = worker.generate_job_key(jid)
        job_info = rd.get(job_key)
        __update_status(job_key, "completed")
        print(job_info)

def __update_status(job_key, status):
    try:
        job = rd.get(job_key)
        job_info = ast.literal_eval(job)
        job_info["status"] = status
        rd.set(job_key, str(job_info))
    except:
        return ("Invalid Request")

execute_job()
