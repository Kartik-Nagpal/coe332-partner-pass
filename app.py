from flask import Flask, jsonify, request
import json, worker

# The main Flask app
app = Flask(__name__)

@app.route('/')
def getData():
    #gets all data
    return (worker.add_job(0, 100) + "\t Sent\n")


@app.route('/status/<string:job_key>')
def getJobStatus(job_key):
    return (worker.get_status(job_key) + "\n")


@app.route('/jobsWIP', methods=['POST'])
def jobs_api():
    try:
        job = request.get_json(force=True)
    except Exception as e:
        return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)}) #something
    return json.dumps(worker.add_job(job['start'], job['end']))
